from tkinter import *
import tkinter as tk
from tkinter import PhotoImage
from tkinter import ttk
from tkinter import messagebox
from pymongo import MongoClient
import random


root = Tk()
root.title("Gym Management")
root.geometry("650x400+420+180")

background_image = PhotoImage(file = "background.png")
main_frame = Frame(root)
main_frame.pack(fill="both", expand=True)
background_label = Label(root, image=background_image)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

label = Label(root, text = "Welcome to InfinityGains Gym !!!",width=30,font=("bold", 20), fg="white", bg="black")
label.place(x=100, y=50)

# Replace these values with your MongoDB server information
mongo_client = MongoClient("mongodb://localhost:27017/")
db = mongo_client["gym_management"]  # Use your desired database name
collection = db["members"]  # Use your desired collection name

def new():
    top1 = Toplevel()
    top1.title("Member Information")
    top1.geometry("650x400+420+180")

    label = Label(top1, text = "Enter New Member Information.",width=30,font=("bold", 20))
    label.place(x=100, y =10)

    def generate_gym_id():
        num =""
        for i in range(5):
            num += str(random.randint(0,9))
        return num

    entry_name = StringVar()
    entry_address = StringVar()
    entry_phone = StringVar()
    entry_email = StringVar()
    entry_dob = StringVar()
    gender_var = StringVar()
    combo_gym_cardio = StringVar()
    combo_personal_training = StringVar()
    payment_var = StringVar()
    
    gym_id_label = Label(top1, text="Gym ID:")
    gym_id_label.place(x=10, y=310)

    gym_id_value = Label(top1, text=generate_gym_id())
    gym_id_value.place(x=100, y=310)
        
    name = Label(top1, text = "Name")
    name.place(x =10, y=80)
    entry1 = Entry(top1, textvariable= entry_name).place(x=100, y=80)

    address = Label(top1, text= "Address:")
    address.place(x =10, y=120)
    entry2 = Entry(top1, textvariable= entry_address).place(x=100, y=120)

    phone_no = Label(top1, text= "Phone No:")
    phone_no.place(x =10, y=160)
    entry3 = Entry(top1, textvariable= entry_phone).place(x=100, y=160)

    email_id = Label(top1, text= "Email ID:")
    email_id.place(x =10, y=200)
    entry4 = Entry(top1, textvariable= entry_email).place(x=100, y=200)

    dob = Label(top1, text = "DOB:")
    dob.place(x=10, y= 240 )
    entry5 = Entry(top1, textvariable= entry_dob).place(x=100, y=240)

    gender = Label(top1, text = "Gender:")
    gender.place(x=10, y = 270)

    radio_button_male = ttk.Radiobutton(top1, text='Male', variable= gender_var, value="Male")
    radio_button_male.place(x= 100 , y= 270)
    radio_button_female = ttk.Radiobutton(top1, text='Female', variable= gender_var, value="Female")
    radio_button_female.place(x= 160 , y= 270)
    
    gym_cardio = Label(top1, text = "Gym & Cardio")
    gym_cardio.place(x=340, y=120)

    combo = ttk.Combobox(top1, textvariable= combo_gym_cardio, values=["-----None-----","Monthly Fees - 500/-", "Quarterly Fees - 1300/-", "Half Yearly Fees - 2500/-", "Yearly Fees - 4500/-"])
    combo.place(x=460, y= 120)

    personal_training = Label(top1, text = "Personal Training")
    personal_training.place(x=340, y=80)

    combo1 = ttk.Combobox(top1, textvariable= combo_personal_training, values=["-----None-----","Monthly Fees - 3000/-", "Quarterly Fees - 8000/-", "Half Yearly Fees - 15000/-", "Yearly Fees - 25000/-"])
    combo1.place(x=460, y= 80)
    
    cash = ttk.Radiobutton(top1, text = "Cash", variable=payment_var, value = "Cash")
    cash.place(x = 500, y = 160)

    gpay = ttk.Radiobutton(top1, text = "GPay", variable=payment_var, value = "gpay")
    gpay.place(x = 360, y = 160)

    cheque = ttk.Radiobutton(top1, text = "Cheque", variable=payment_var, value = "cheque")
    cheque.place(x = 425, y = 160)

    
    def submit():
        member_info = {
        "gym_id": gym_id_value.cget("text"),
        "name": entry_name.get(),
        "address": entry_address.get(),
        "phone_no": entry_phone.get(),
        "email_id": entry_email.get(),
        "dob": entry_dob.get(),
        "gender": gender_var.get(),
        "gym_cardio": combo_gym_cardio.get(),
        "personal_training": combo_personal_training.get(),
        "payment_method": payment_var.get()
    }
        collection.insert_one(member_info)
        msg = messagebox.showinfo("Information","Regitration Completed !!!")
        top1.destroy()

    backbtn = Button(top1, text = "Back", command = top1.destroy)
    backbtn.place(x = 260, y = 320)

    submit = Button(top1, text = "Submit", command= submit)
    submit.place(x = 380, y = 320)


def search():
    top2 = Toplevel()
    top2.title("Search Member Information")
    top2.geometry("400x300+420+180")

    back_button = Button(top2, text="Back", command=top2.destroy)
    back_button.place(x=150, y=250)

    search_label = Label(top2, text="Enter Gym ID:")
    search_label.place(x=10, y=10)

    search_enter = Entry(top2)
    search_enter.place(x= 100, y=10)

    search_results = Listbox(top2, width=50, height=12)
    search_results.place(x = 10 , y = 50)

    def find():
        search_results.delete(0, END)
        search_value= search_enter.get()
        search_enter.delete(0, END)

        if search_value:
            query = {"gym_id": search_value}
            results = collection.find(query)

        for result in results:
            search_results.insert(END, f"ID: {result['gym_id']}")
            search_results.insert(END, f"Name: {result['name']}")
            search_results.insert(END, f"Address: {result['address']}")
            search_results.insert(END, f"Phone No: {result['phone_no']}")
            search_results.insert(END, f"Email ID: {result['email_id']}")
            search_results.insert(END, f"DOB: {result['dob']}")
            search_results.insert(END, f"Gender: {result['gender']}")
            search_results.insert(END, f"Gym Cardio: {result['gym_cardio']}")
            search_results.insert(END, f"Personal Training: {result['personal_training']}")
            search_results.insert(END, f"Payment Method: {result['payment_method']}")
            

    search_btn = Button(top2,text="Search",command=find,font=("bold", 10))
    search_btn.place(x=240, y=10)


new_member = Button(root, text = "New Membership",width= 30, command = new,font=("bold", 15))
new_member.place(x=170,y=140)

member_search = Button(root, text = "Search Member Information",width= 30, command=search,font=("bold", 15))
member_search.place(x=170, y=200)

exit = Button(root, text = "Exit",width= 30, command = root.destroy,font=("bold", 15))
exit.place(x=170, y = 260)


root.mainloop()
